<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Customer;

class CustomerLoginController extends Controller
{
    use AuthenticatesUsers;
    // protected $redirectTo = '/customer/home';

    // public function __construct()
    // {
    //     $this->middleware('guest:customer')->except('logout')->except('index');
    // }

    public function index(){
        return view('customer.home');
    }

    public function showLoginForm()
    {
        return view('customer.auth.login');
    }

    public function showRegisterForm()
    {
        return view('customer.auth.register');
    }

    public function username()
    {
            return 'username';
    }

    public function register(Request $request)
    {
        $request->validate([
            'username'      => 'required|string|unique:customers',
            'email'         => 'required|string|email|unique:customers',
            'password'      => 'required|string|min:6|confirmed'
        ]);
       Customer::create([
            'id' => mt_rand(10000000,99999999),
            'username' => $request['username'], 
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
       ]);
        return redirect()->route('customer.registerform')->with('success', 'Successfully register!');
    }
}
