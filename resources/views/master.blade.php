<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Klinik Ratnasari - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <link rel="manifest" href="site.webmanifest"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/css/slicknav.css">
    <link rel="stylesheet" href="/css/flaticon.css">
    <link rel="stylesheet" href="/css/gijgo.css">
    <link rel="stylesheet" href="/css/animate.min.css">
    <link rel="stylesheet" href="/css/animated-headline.css">
	<link rel="stylesheet" href="/css/magnific-popup.css">
	<link rel="stylesheet" href="/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="/css/themify-icons.css">
	<link rel="stylesheet" href="/css/slick.css">
	<link rel="stylesheet" href="/css/nice-select.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <!-- ? Preloader Start -->
    {{-- <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="/img/logo/logo.png" alt="">
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Preloader Start -->
<header>
    <!--? Header Start -->
    @include('path.header')
    <!-- Header End -->
</header>
<main>
    @yield('content')
</main>
    <footer>
        <!--? Footer Start-->
        <div class="footer-area">
            <div class="container">
                <div class="footer-top footer-padding">
                    <div class="row d-flex justify-content-between">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-8">
                            <div class="single-footer-caption mb-50">
                                <!-- logo -->
                                <div class="footer-number">
                                   <h4>Klinik Ratnasari</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. 
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-5">
                            <div class="single-footer-caption mb-50">
                                <div class="footer-about">
                                    <h4>Tentang Kami</h4>
                                        <a href=""><p class="info1">Jadwal Dokter</p></a>
                                        <a href=""><p class="info1">Galeri</p></a>
                                        <a href=""><p class="info1">Karir</p></a>
                                        <a href=""><p class="info1">Cabang</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-8">
                            <div class="single-footer-caption mb-50">
                                <div class="footer-number mb-50">
                                    <h4>Sosial Media</h4>
                                     <i class="fab fa-facebook"></i>
                                     <a href=""><p>Instagram</p></a>
                                     <a href=""><p>Youtube</p></a>                        
            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row ">
                        <div class="col-xl-9 col-lg-8">
                            <div class="copy-right text-center">
                                <strong> <p>&copy;<script>document.write(new Date().getFullYear());</script> - Klinik Ratnasari Sehat Group || Powered by Klinik Sehat Ratna Sari</p> </strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
    </footer>
    <!-- Scroll Up -->
    <div id="back-top" >
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>
    <div id="wa-contact" >
        <a title="Go to WA" href="https://web.whatsapp.com/send?phone=6285793845435&text=Hai%20Klinik%20Ratnasari%0A%0A%2ANama%20Saya%20%3A%2A%20...%0A%2AKeperluan%20Saya%20%3A%2A%20..." target=”_blank”> <i class="fab fa-whatsapp"></i></a>
    </div>

    <!-- JS here -->
    <script src="https://unpkg.com/feather-icons"></script>
    <script src="/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="/js/wow.min.js"></script>
    <script src="/js/animated.headline.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
    
    </body>
</html>