@extends('master')

@section('title', 'Cabang')

@section('content')
<div class="department_area section-padding2">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center mb-100">
                    <span>Cabang</span>
                    <h2>Cabang Klinik Ratnasari</h2>
                </div>
            </div>
        </div>

        <div class="dept_main_info white-bg">
                   <div class="row col-lg-12 mb-30">
                        <div class="col-lg-6 mb-30">
                                <h3> <b> 1. Cabang 1 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                        <div class="col-lg-6">
                            <h3> <b> 2. Cabang 2 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                    </div>
                    <div class="row col-lg-12 mb-30">
                        <div class="col-lg-6 mb-30">
                                <h3> <b> 3. Cabang 3 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                        <div class="col-lg-6 mb-30">
                            <h3> <b> 4. Cabang 3 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                    </div>
                    <div class="row col-lg-12 mb-30">
                        <div class="col-lg-6 mb-30">
                                <h3> <b> 5. Cabang 5 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                        <div class="col-lg-6">
                            <h3> <b> 6. Cabang 6 </b></h3 > <br>
                                <p>Alamat: </p>
                                <p>No. Tlp: </p>
                                <p>Google Map: </p>
                        </div>
                    </div>

    </div>
</div>
  
@endsection