<div class="header-area">
    <div class="main-header header-sticky">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Logo -->
                <div class="col-xl-2 col-lg-2 col-md-1">
                    <div class="logo">
                        <a href="/"><img src="/img/logo/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-10 col-md-10">
                    <div class="menu-main d-flex align-items-center justify-content-end">
                        <!-- Main-menu -->
                        <div class="main-menu f-right d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a href="/" class="nav-link active"> <i class="fa fa-home"></i> Home</a></li>
                                    <li><a href="/profil"><i class="fa fa-user"></i> Profil</a></li>
                                    <li><a href="/layanan"><i class="fa fa-plus-square"></i> Layanan</a></li>
                                    <li><a href="/cabang"><i class="fa fa-sitemap"></i> Cabang</a></li>
                                    <li><a href="/artikel-testimoni"><i class="fa fa-list"></i> Artikel & Testimoni</a>
                                        <ul class="submenu">
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog_details.html">Blog Details</a></li>
                                            <li><a href="elements.html">Element</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/kontak"><i class="fa fa-phone"></i> Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="header-right-btn f-right d-none d-lg-block ml-30">
                            <a href="{{ route('customer.loginform') }}" class="btn header-btn">Member Area</a>
                        </div>
                    </div>
                </div>   
                <!-- Mobile Menu -->
                <div class="col-12">
                    <div class="mobile_menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
</div>