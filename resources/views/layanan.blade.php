@extends('master')

@section('title', 'Layanan')

@section('content')
<div class="services_area section-padding2">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center mb-100">
                    <span>Layanan </span>
                    <h2>Klinik Umum</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="services_tab mb-30">
                    <!-- Tabs Buttons -->
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link active">
                                <i class="flaticon-teeth"></i>
                                <h4>Layanan 1</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" >
                                <i class="flaticon-cardiovascular"></i>
                                <h4>Layanan 2</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">
                                <i class="flaticon-ear"></i>
                                <h4>Layanan 3</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">
                                <i class="flaticon-bone"></i>
                                <h4>Layanan 4</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" >
                                <i class="flaticon-lung"></i>
                                <h4>Layanan 5</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">
                                <i class="flaticon-cell"></i>
                                <h4>Layanan 6</h4>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dept_main_info white-bg">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                <a href="#" class="dep-btn">Appointment<i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="" height="500px">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                <a href="#" class="dep-btn">Appointment<i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                <div class="tab-pane fade" id="Astrology" role="tabpanel" aria-labelledby="Astrology-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                <a href="#" class="dep-btn">Appointment<i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                <div class="tab-pane fade" id="Neuroanatomy" role="tabpanel" aria-labelledby="Neuroanatomy-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                <a href="#" class="dep-btn">Appointment<i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                <div class="tab-pane fade" id="Blood" role="tabpanel" aria-labelledby="Blood-tab">
                    <!-- single_content  -->
                    <div class="row align-items-center no-gutters">
                        <div class="col-lg-7">
                            <div class="dept_info">
                                <h3>Dentist with surgical mask holding <br> scaler near patient</h3 >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                                <a href="#" class="dep-btn">Appointment<i class="ti-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="dept_thumb">
                                <img src="/img/gallery/department_man.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- single_content  -->
                </div>
                </div>
        </div>

    </div>
</div>
  
@endsection