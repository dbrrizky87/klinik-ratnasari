<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/','AdminController@admin' )->middleware('auth');
// Route::post('/admin-login','LoginController' );

Route::get('/','HomeController@index' );

Route::get('/profil/', function () {
    return view('profil');
});
Route::get('/layanan/', function () {
    return view('layanan');
});
Route::get('/cabang/', function () {
    return view('cabang');
});

Route::get('/artikel-testimoni/','ArticleController@index' );
Route::get('/kontak/', function () {
    return view('kontak');
});

Route::get('/customer/login', 'CustomerLoginController@showLoginForm')->name('customer.loginform');
Route::get('/customer/register', 'CustomerLoginController@showRegisterForm')->name('customer.registerform');
Route::post('/customer/login', 'CustomerLoginController@login')->name('customer.login');
Route::post('/customer/register', 'CustomerLoginController@register')->name('customer.register');
Route::get('/customer/home', 'CustomerLoginController@index')->middleware('auth:customer');
Route::get('/customer/logout', 'CustomerLoginController@logout')->name('customer.logout');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
